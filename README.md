# MOCCASIN beta version 0.25 user guide

This is a script for splicing confounding factor adjustment for use with [MAJIQ](https://majiq.biociphers.org/) 2.1. The script is called Modeling Confounding Factors Affecting Splicing Quantification (MOCCASIN).
After correcting for read depth, the program models the variation in junction read rates due to confounding factors and covariates.
It then adjusts the read rates to remove variation due to confounding factors and outputs updated tables of junction read rates.

Concretely, the program takes in a user-supplied model matrix and .majiq files from a MAJIQ build (Vaquero-Garcia J. et al 2016, Norton S. et al 2017) or properly formatted numpy files of count data (see: Python API usage below). 
It outputs new .majiq files with adjusted junction read rates or adjusted numpy files with adjusted counts. The adjusted MAJIQ files can be utilized with the MAJIQ quantifier.

## Dependencies

The following (standard) python packages are required: 
argparse, datetime, numpy, scipy, pandas, math, sys, os, shutil, multiprocessing

MOCCASIN has been tested with python 3.8.6 and MAJIQ builder (.majiq) files from MAJIQ 2.1.

## Brief method description

MOCCASIN adjusts the junction reads for each sample (.majiq file) to remove variation due to known (and optionally unknown) confounding factors.

The main inputs to MOCCASIN are a model matrix and a list of confounding factors (subset of the model matrix column names). 

MOCCASIN uses the model matrix to model and remove variation in junction reads associated with the indicated confounding factors. 
The main algorithm is summarized below; additional detail follows, and detailed methods are linked above.

	PROCEDURE MOCCASIN:

		A. (Optional) Augment the model matrix by modeling variation due to unknown confounding factors. 
			This procedure is influenced heavily by RUVSeq (Risso D. et al, Nat Biotechnol. 2014).
			Additional factors are discovered corresponding to variation not explained by the user's known confounders or covariates.

			1. Collect the junction reads from all samples into a table.
				(Summarize the bootstrap sets by taking a median over bootstraps.)
			2. Normalize the junction reads to PSI (proportion spliced in) for each sample and LSV.
				If there are no reads for an LSV in a sample, each junction PSI is set to 0 in this step.
			3. Model and remove variation explained by the main variables of interest (e.g. case/control), leaving residual values.
			4. Perform a factor analysis on the resulting data table to infer per-sample factor scores corresponding to sources of unwanted variation.
			5. Append the new factors to the model matrix and continue.

		B. For each bootstrap set:
			(MAJIQ builder samples read rates for each LSV junction by bootstrapping over junction-adjacent positions. 
			MOCCASIN models and corrects the junction read rates separately for each bootstrap set. Builder uses 30 sets by default.)

			1. Collect the junction reads from all samples into a table.
			2. Normalize the junction reads to PSI (proportion spliced in) for each sample and LSV.
				If there are no reads for an LSV in a sample, each junction PSI is set to 0 in this step.
			3. Separately for each junction, use the model matrix to perform ordinary least squares regression on PSI.
				By default, the variation explained by the non-confounders is modeled and retained (non-default: see --full_correction.)
			4. PSI values are adjusted after modeling the contributions of the confounding factors.
				The model matrix entries are adjusted to remove any differential influence of the confounders between samples.
				The entries for confounders are set to 0 by default, but other constants can be specified in the call to MOCCASIN.
			5. Adjusted read-rates are computed by multiplying adjusted PSI into the initial total coverage for that LSV and sample
			    so that the original number of reads is distributed over the junctions.
				Default: the target coverage is the total initial coverage, so that the original number of reads is distributed over the junctions. 

		Finally, the adjusted junction reads from each bootstrap set are collected and output as new .majiq files corresponding to each sample.

## Model Matrix

The user-supplied model matrix will be used in ordinary least squares regression on PSI calculated from the junction read-rates in the builder files. For each junction in each LSV, an OLS regression model is fit to the PSI values. Then, the model matrix is updated so that the confounding factor scores are constant (default: 0) over all samples. The updated model matrix is used to compute adjusted junction counts.

Due to the use of OLS regression, factors (e.g. batch ID) should be included as binary indicator columns (0 or 1) rather than integer factor labels (1, 2, 3...).

The model matrix should be in a tab-delimited file (tsv).

The model matrix should have the following format:

- The first row contains the column names, tab-separated. (Avoid extra tabs!)
- The first column must have name "filename". 
- The filename entries should be the names of the MAJIQ files in the build directory to be utilized in the analysis, without any file extension (e.g., .majiq).
- The subsequent column names should correspond to confounding factors (e.g. batch name) or covariates. Column names should not contain spaces. Column names must include at least one alphabetic character (letter, rather than number).
- The user should explicitly include an intercept column in the model matrix if it is desired to model using an intercept. MOCCASIN never adds an intercept automatically.
- The model matrix must be full-rank.
	When an intercept is included, this means one factor label must be left out for each factor.
	For example, if there are three batches, then the indicator column for one of the three must be omitted for the matrix to remain full-rank. This effectively requires the user to select a reference case. The adjustment results may depend upon the choice of reference case.

Model matrix outline example: ("modelmat.tsv")

	filename	batch_1	batch_3	gender	has_effect
	sample_1	1	0	1	1
	sample_2	1	0	0	1
	sample_3	0	0	1	0
	...

In this example, the model matrix refers to sample_1.majiq, sample_2.majiq, etc. The containing directory must be given as a positional argument. (There should be no leading tab-indentation in the model matrix tsv file, that's just here for markdown.)

## Special model matrix column names (reserve words)

These column names, when utilized, are reserve words which are treated in a special way by MOCCASIN.

### filename

Optional or required: REQUIRED

The filename column should be included as described above, in the "Model Matrix section."

### intercept

Optional or required: OPTIONAL

Usually a constant 1 for each sample. Currently, this is only used explicitly in the --full_correction option.

It is not necessary to use an intercept with MOCCASIN; the same effects can be accomplished by appropriately setting the adjustment values for the confounding factors (see "Confounding Factors").

### W_1, W_2... W_k for positive integer k

NEVER USE.

If the -U option is used to discover additional factors of unwanted variation, those factors will be given column name W_1, W_2, ... W_k (k=number of factors). Therefore the user should not use these column names in the model matrix.

## Confounding Factors

MOCCASIN is called with a space-delimited list of confounding factors, with optional constant replacement value following each factor name. 

Each confounding factor is a column name from the model matrix.

The optional constant value following a confounding factor name corresponds to its replacement value. If omitted, this is set to 0, which corresponds to removing the confounding factor from the model matrix when computing adjusted junction read values. By setting this value, the user can specify the desired "adjustment case", i.e. setting a "male" factor to 1 means the output corresponds to (modeled) male individuals, setting a "RIN" factor to 10 means the output corresponds to modeled high-qualty samples, etc.

The list may have zero elements (e.g. if it is desired to perform adjustment using only RUV factors), or up to the number of factors included in the user's model matrix. RUV factors are always set to 0 for adjusting counts.

Example for this command line argument:

	[nothing, possibly when using -U flag]
	batch1 					# batch1 effect is removed in adjustment
	batch1 1 				# batch1 effect is enforced in adjustment
	batch1 1 male 			# batch1 effect is enforced, male gender effect is removed in adjustment
	batch1 1 male RIN 10	# batch1 effect enforced, male effect removed, RIN=10 enforced in adjustment

In the above examples, all given confounding factor names must be column names in the provided model matrix (see "Model Matrix").

Note that, as implied above, it is the user's responsibility to select a desirable "adjustment" case. For example, if the data are in batch1 and batch2, and both are removed (set to 0) for adjustment, then the output data will represent batch-less samples, which probably is not the desired result. It may be better to select a batch to set to 1 as the "adjustment" batch. Likewise, if RIN is set to 0, then the output data will represent (modeled) low-quality samples.

## Output

MOCCASIN outputs one adjusted .majiq file for each input file. The output files have the same name as the input file, but go in the output directory (positional argument) and the output for [filename].[extension] is [filename].moccasin_adj.[extension]. For example, the output for sample_1.majiq is sample_1.moccasin_adj.majiq. (SCF = splicing confounding factors) 

## Limitations

MOCCASIN assumes all input .majiq files have coverage tables with the same number of columns, i.e. the same number of bootstrap sets. Current behavior is undefined (may crash) if this condition does not hold (as for e.g. incremental builds with different numbers of bootstrap sets.)

MOCCASIN does not assume that all LSVs are in all input majiq files. However, only LSVs which are in all files will be adjusted for confounding factors. LSVs which are not detected in all files are left unchanged in the files for which they are detected.

## Program call

The help message (-h) follows:                                                                 

	usage: moccasin.py [-h] [-E MAJIQ_EXTENSION] [-J MAX_NUM_PROCESSES] [-X]
	                   [-U RUV_MAX_NUM_FACTORS] [-V RUV_MAX_NUM_JUNCTIONS] [-K]
	                   [-Q] [-F] [-M MAX_NCELLS_PER_CHUNK] [-C] [-S] [--version]
	                   model_matrix majiq_build_dir out_dir
	                   [confounding_factors [confounding_factors ...]]

	positional arguments:
	  model_matrix          Path to the model matrix tsv.
	  majiq_build_dir       Path to the directory containing output files from a
	                        majiq build. The build directory should contain all
	                        the majiq files to be utilized (including, but not
	                        neccessarily limited to, all those named in the model
	                        matrix file).
	  out_dir               Path to the output directory; will be created if it
	                        does not already exist.
	  confounding_factors   Confounding factors; subset of the model matrix column
	                        headers. A space-delimited list without brackets. Each
	                        factor may optionally be followed by the desired
	                        numerical adjustment value for that factor (default:
	                        0)

	optional arguments:
	  -h, --help            show this help message and exit
	  -E MAJIQ_EXTENSION, --majiq_extension MAJIQ_EXTENSION
	                        File extension of the input .majiq files. (default:
	                        .majiq)
	  -J MAX_NUM_PROCESSES, --max_num_processes MAX_NUM_PROCESSES
	                        Max number of processes to use. (default: 1)
	  -X, --explore_residual_factors
	                        Print the variance explained by principal components
	                        of the residuals. When set, no adjustment will be
	                        performed: the variances will be printed and the
	                        program will exit. This can be used to help determine
	                        how many factors should be used with RUV. When set, -U
	                        also should be set to the max number of factors that
	                        should be printed.
	  -U RUV_MAX_NUM_FACTORS, --RUV_max_num_factors RUV_MAX_NUM_FACTORS
	                        Max number of RUV factors to find and use. (default:
	                        0)
	  -V RUV_MAX_NUM_JUNCTIONS, --RUV_max_num_junctions RUV_MAX_NUM_JUNCTIONS
	                        Max number of junctions input to RUV for discovering
	                        factors of unwanted variation. The most-varying
	                        junction is considered for each LSV. From those, only
	                        up to the max are selected. (default: 10000)
	  -K, --keep_tmp_dir    Keep the temporary directory created by this module,
	                        rather than deleting during cleanup.
	  -Q, --quiet           Do not print updates to stdout.
	  -F, --full_correction
	                        When using ordinary least squares regression to adjust
	                        counts, model the confounding factors only, without
	                        the non-confounding factors as covariates. This option
	                        results in a possibly stronger adjustment which may
	                        remove more variation associated with the main
	                        variables of interest. In this case, the non-
	                        confounders should be included in the model matrix
	                        only if desired together with the -U option. (default:
	                        false, i.e. model the non-confounding factors too)
	  -M MAX_NCELLS_PER_CHUNK, --max_ncells_per_chunk MAX_NCELLS_PER_CHUNK
	                        Max number of coverage table cells allowed per chunk.
	                        Junctions are partitioned into multiple chunks to
	                        ensure low peak memory use. (default: 1e07)
	  -S, --stop_before_modeling
	                        (Diagnostic) End the execution before the modeling
	                        step.
	  --version             show program's version number and exit


## Python API Usage

Using Moccasin as a module directly from a python program, without using a subprocess-style call, is also supported.

This effectively allows moccasin to adjust coverage from data provided by other tools, besides Majiq. 
 
The quickstart with basic functionality is shown here. The config matrix / file normally provided here is a python
dict called 'model_defs', and the usual confounding factor arguments are listed as another python dict called 
'confounding_factors'. Note that you can look in the 'examples' directory for a more complete example usage / script.

    from moccasin import MoccasinModel
    
    model_defs = {
        'workshop_Cer3': {"intercept": 1, "example_batch": 0},
        'workshop_Adr2': {"intercept": 1, "example_batch": 1}
    }
    
    confounding_factors = {
        'example_batch': 0
    }
    
    model = MoccasinModel(
        model_defs,
        confounding_factors=confounding_factors,
        max_num_processes=12
    )
    
    # model with multi dimensional array, with the second axis as bootstrap samples, similar to majiq:
    
    output = model.model_with_bootstraps(
        grouping, {
            'workshop_Cer3': coverage_array,
            'workshop_Adr2': coverage_array,
        }
    )
    
    # model directly without bootstraps, on 1d coverage arrays
    output = model.model_with_bootstraps(
        grouping, {
            'workshop_Cer3': coverage_array,
            'workshop_Adr2': coverage_array,
        }
    )
    


## Technical notes

With respect to the input .majiq files, MOCCASIN updates the coverage table values (junction read rates) and possibly the order of the junction rows in the coverage table and corresponding order of the junc_info table entries. If the -C option is used, MOCCASIN additionally updates the num_reads and num_positions fields in the junc_info table.

.majiq files contain duplicate entries for junctions which are in more than one LSV (e.g. a single-source and a single-target LSV). MOCCASIN adjusts these duplicate rows independently. This may result in different adjusted values for the same splice junctions, in contrast to a MAJIQ builder output. This highlights the LSV-centric viewpoint of MOCCASIN.

## Final Notes

Contact yosephb@upenn.edu with comments or questions.

## Change log

		0.25	First public relase
		
## License
The MIT License (MIT)
Copyright (c) 2020, University of Pennsylvania

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

## References

Norton S. et al, "Outlier detection for improved differential splicing quantification from RNA-Seq experiments with replicates." Bioinformatics, Vol 34, Issue 9:1488â€“1497. https://doi.org/10.1093/bioinformatics/btx790

Risso D. et al, â€œNormalization of RNA-seq data using factor analysis of control genes or samples.â€ Nat Biotechnol. 2014 Sep; 32(9): 896â€“902. https://doi.org/10.1038/nbt.2931

Vaquero-Garcia J. et al, "A new view of transcriptome complexity and regulation through the lens of local splicing variations". ELife 2016;10.7554/eLife.11752 â€‹ https://doi.org/10.7554/eLife.11752.001