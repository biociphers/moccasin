##########################################################################
#
# MOCCASIN: Modeling Confounding Factors Affecting Splicing Quantification
#
# This module adjusts a majiq build for known and unknown confounders.
# The user supplies a model matrix, lists known and unknown confounders,
# and selects the max number of unknown confounding factors to use. 
# The main method is at the of this file. The basic call structure is:
#
#
# main
# - get_consolidate_arguments_and_vars
# - mkdirs
# - get_model_matrix
# - get_LSVs
# - get_transition_d
# - get_worker_coverage_row_idxs
# - augment_modelmat_with_unknown_cf_covariates
# --- get_ruv_bootstrap_sample_df
# --- get_std_df
# ----- LSV_sum_reexpand
# --- get_ruv_bootstrap_sample_df
# --- get_std_df
# --- RUV_imitator
# - split_coverage_tables
# - get_proc_pool
# - fitmodel_adjustcounts
# --- get_linreg_modelmat_df
# --- fitmodel_adjustnreadsnpos_cj
# ----- get_cj_nreadsnpos_arrs
# ----- fitmodel_adjust_counts_linear
# --- fitmodel_adjustcounts_cij
# ----- get_cij_coverage_df
# ----- fitmodel_adjust_counts_linear
# ----- LSV_sum_reexpand
# ----- fitmodel_adjust_counts_linear
# ----- LSV_sum_reexpand
# - get_write_new_majiqs
# --- assemble_adjusted_coverage_kcj
#
#
# A weak indexing convention used throughout is:
# - c: chunk index (subset of common rows in the coverage table)
# - k: row index for the model matrix
# - i: Majiq builder bootstrap index; coverage table column index
# - j: worker index, for multiprocessing (j'th spawned process)
#
#
# A weak naming convention used throughout is that some variables names
# include a suffix describing the type:
# - df: pandas dataframe
# - arr: numpy array
# - d: dictionary
# - s: set
# - l: list
#
#
# This code assumes all input .majiq files have coverage tables with the
# same number of columns, i.e. the same number of bootstrap sets. 
# Current behavior is undefined (may crash) if this condition does not hold.
#
# This code does not assume that all junctions are in all input majiq
# files. However, only junctions which are in all files will be adjusted
# for confounding factors. Junctions which are not detected in all files
# are left unchanged.
#
#
##########################################################################

### Remote modules
from os import mkdir
from shutil import rmtree
import multiprocessing

### Local modules
from moccasin.scf_args import *
from moccasin.unknown_confounders import *
from moccasin.fit_adjust import *
from moccasin.prep_output import *


__version__ = "0.24"


def mkdirs():
	"""Return None

	Create the output directory and the nested temporary directory.
	"""

	for f_dir in [ conf.out_dir, conf.tmp_dir ]:
		try:
			mkdir( f_dir )
		except OSError as e:
			print("Exception: "+str(e))

	return None


def write_vars_to_logfile(d, logfile):

	K = sorted( list( d.keys() ) )

	for k in K:
		logfile.write( "%s\t%s\n"%(str(k), str(d[k])) )

	logfile.flush()

	return None


def get_proc_pool(k):
	"""Return a multiprocessing.Pool

	Return a pool with number of workers specified in a dictionary key-value.
	"""

	num_workers = getattr(conf, k)

	mproc_pool = None

	if num_workers > 1:
		print("Creating pool with %i workers."%(num_workers))
		mproc_pool = multiprocessing.Pool( num_workers )

	return mproc_pool


args = get_args(__version__)
get_consolidate_arguments_and_vars(args)

if __name__ == "__main__":
	"""main method. The program flow is described in additional
	detail at the top of this file.
	"""

	args = get_args(__version__)

	start_time = datetime.now()

	mkdirs()

	multiprocessing.set_start_method(method="spawn")

	#write_vars_to_logfile(d, logfile)

	(modelmat_df, n_samples, build_model_samples) = get_model_matrix(file_path=conf.model_matrix)

	no_ruv = conf.RUV_max_num_factors <= 0 and not conf.explore_residual_factors
	if not no_ruv:
		modelmat_df = augment_modelmat_with_unknown_cf_covariates(modelmat_df,
			n_samples, build_model_samples)

	(LSVs, junc_row_order_d, n_bootstraps) = get_LSVs(modelmat_df)

	transition_d = get_transition_d(modelmat_df, junc_row_order_d, LSVs)

	num_common_juncs = len(LSVs)

	chunks_d = get_worker_coverage_row_idxs(LSVs,
		num_common_juncs, n_samples)

	split_coverage_tables(modelmat_df, chunks_d,
		num_common_juncs, transition_d)

	if conf.stop_before_modeling:
		sys.exit() # diagnostic

	mproc_pool = get_proc_pool("max_num_processes")

	fitmodel_adjustcounts(chunks_d, modelmat_df, mproc_pool,
		n_bootstraps, build_model_samples)

	if mproc_pool is not None:
		mproc_pool.close()

	get_write_new_majiqs(chunks_d, modelmat_df,
		transition_d, n_bootstraps)

	del chunks_d

	print("Done!")

	end_time = datetime.now()

	if not conf.keep_tmp_dir:
		rmtree(conf.tmp_dir)