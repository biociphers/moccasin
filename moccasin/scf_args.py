import argparse
from os.path import join
from moccasin import conf
from moccasin.scf_util import *


def get_args( __version__ ):
	"""Return parsed arguments"""

	parser = argparse.ArgumentParser()

	### Optional arguments ###

	parser.add_argument("-E", "--majiq_extension", type=str, default=".majiq", 
		help="File extension of the input .majiq files. (default: .majiq)")

	parser.add_argument("-J", "--max_num_processes", type=int, default=1, 
		help="Max number of processes to use. (default: 1)")

	parser.add_argument("-X", "--explore_residual_factors", action="store_true", 
		help="Print the variance explained by principal components of the residuals. \
		When set, no adjustment will be performed: the variances will be printed and \
		the program will exit. This can be used to help determine how many factors \
		should be used with RUV. When set, -U also should be set to the max number of \
		factors that should be printed.")

	parser.add_argument("-U", "--RUV_max_num_factors", type=int, default=0, 
		help="Max number of RUV factors to find and use. (default: 0)")

	parser.add_argument("-V", "--RUV_max_num_junctions", type=int, default=10000, 
		help="Max number of junctions input to RUV for discovering factors\
		of unwanted variation. The most-varying junction is considered for each\
		LSV. From those, only up to the max are selected. (default: 10000)")

	parser.add_argument("-K", "--keep_tmp_dir", action="store_true",
		help="Keep the temporary directory created by this module, rather than deleting during cleanup.")

	parser.add_argument("-Q", "--quiet", action="store_true", 
		help="Don't print updates to stdout.")

	parser.add_argument("-F", "--full_correction", action="store_true", 
		help="When using ordinary least squares regression to adjust counts, model the confounding \
		factors only, without the non-confounding factors as covariates. \
		This option results in a possibly stronger \
		adjustment which may remove more variation associated with the main variables of interest. \
		In this case, the non-confounders should be included in the model matrix only if desired \
		together with the -U option. \
		(default: false, i.e. model the non-confounding factors too)")

	parser.add_argument("-M", "--max_ncells_per_chunk", type=int, default=int( np.power(10, 7) ), 
		help="Max number of coverage table cells allowed per chunk. Junctions are \
		partitioned into multiple chunks to ensure low peak memory use. (default: 1e07)")

	parser.add_argument("-C", "--adjust_coverage", action="store_true", 
		help="If set, MOCCASIN will adjust the MAJIQ builder output for coverage level in \
		addition to the default adjustment of junction coverage ratios for each LSV. \
		This option applies an adjustment to num_reads and num_pos in the junc_info table \
		of each MAJIQ file, then applies an additional factor correction to the read rates \
		for each LSV to reflect the adjusted coverage. (default: no coverage-level updates)")

	parser.add_argument("-S", "--stop_before_modeling", action="store_true", 
		help="(Diagnostic) End the execution before the modeling step.")

	parser.add_argument("-O", "--common_lsvs_only", action="store_true", 
		help="If set, MOCCASIN will only attempt to model LSVs detected in all input samples, \
		and will not adjust values for LSVs dected only in some samples. \
		(default: Attempt to model the union over all samples, subject to missing values and corresponding model matrix rank.)")

	### Positional arguments ###

	parser.add_argument("model_matrix", type=str, 
		help="Path to the model matrix tsv.")

	parser.add_argument("majiq_build_dir", type=str, 
		help="Path to the directory containing output files from a majiq build.\
		The build directory should contain all the majiq files to be utilized \
		(including, but not neccessarily limited to, all those named in the model matrix file).")

	parser.add_argument("out_dir", type=str, 
		help="Path to the output directory; will be created if it does not already exist.")

	parser.add_argument("confounding_factors", nargs='*', type=str, 
		help="Confounding factors; subset of the model matrix column headers.\
		A space-delimited list without brackets. Each factor may optionally be followed \
		by the desired numerical adjustment value for that factor (default: 0)")

	parser.add_argument("--minimum_reads", default=10, type=int,
		help="Analogous to MAJIQ quantifiable filter. "
			 "Minimum number of reads an LSV should have to be considered quantifiable. Default: 10. "
			 "Set to 0 for pre v0.24 behaviour.")

	parser.add_argument("--minimum_pos", default=3, type=int,
		help="Analogous to MAJIQ quantifiable filter. "
			 "Minimum number of read positions an LSV should have to be considered quantifiable. Default: 3. "
			 "Set to 0 for pre v0.24 behaviour.")

	parser.add_argument("--linear-model-type", default='linear', type=str, choices=['linear', 'stzl'],
						help="Model type to use. default: linear")

	### Version ###

	parser.add_argument("--version", action="version", version="%(prog)s "+__version__)
	
	return parser.parse_args()


def get_consolidate_arguments_and_vars(args):
	"""Return a dictionary

	Stores the arguments and some other variables in a dictionary.
	This dictionary is effectively the global namespace for this module,
	which is helpful for multiprocessing.
	"""

	### Optional arguments

	conf.majiq_extension = args.majiq_extension

	conf.max_num_processes = args.max_num_processes

	conf.explore_residual_factors = args.explore_residual_factors

	conf.RUV_max_num_factors = args.RUV_max_num_factors

	conf.RUV_max_num_junctions = args.RUV_max_num_junctions

	conf.keep_tmp_dir = args.keep_tmp_dir

	conf.quiet_mode = args.quiet

	conf.full_correction = args.full_correction

	conf.MAX_N_CELLS_PER_CHUNK = args.max_ncells_per_chunk

	conf.adjust_coverage = args.adjust_coverage

	conf.stop_before_modeling = args.stop_before_modeling

	conf.common_lsvs_only = args.common_lsvs_only

	conf.minimum_reads = args.minimum_reads

	conf.minimum_pos = args.minimum_pos

	### Positional arguments

	conf.model_matrix = args.model_matrix

	conf.majiq_build_dir = args.majiq_build_dir

	conf.out_dir = args.out_dir

	conf.confounding_factors = process_cf(args.confounding_factors)

	### Additional files and directories

	conf.logfile = join(conf.out_dir, "scf.log")

	conf.tmp_dir = join(conf.out_dir, "tmp")

	conf.model_type = args.linear_model_type


def process_cf(confounders):
	"""
	Return the confounding factor dictionary, which has 
	key: column name (confounding factor)
	value: constant value to which the factor should be set in correction. 
		Defaults to zero for each factor, if another value was not supplied.
	"""

	L = len(confounders)

	cf_d = dict()

	i=0
	while i < L:

		this_ele = confounders[i]

		if i==L-1:
			cf_d[ this_ele ] = 0
			i += 1

		else:
			next_ele = confounders[i+1]
			try:
				float(next_ele)
				cf_d[ this_ele ] = float(next_ele)
				i += 2
			except:
				cf_d[ this_ele ] = 0
				i += 1

	return cf_d