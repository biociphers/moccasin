import numpy as np
from datetime import datetime
import math
from moccasin import conf
import pandas as pd

NATURAL_LOG_2 = math.log(2)
COEFF_B = (1 - math.log(2)) / 4
COEFF_C = (2*math.log(2) - 1) / 2

def get_apply_nf(coverage_df, LSVs):
	"""Return a tuple of two numpy arrays

	nf_df.values: per-sample, per-lsv normalization factors

	coverage_df.multiply( nf_df ).values: coverage table adjusted by normalization factors
	"""

	### Compute per-LSV coverage    
	coverage_df["LSV"] = LSVs
	perlsv_coverage_df = coverage_df.groupby("LSV", sort=False).sum()

	coverage_df = coverage_df.drop("LSV", axis=1)
		
	# Get reference sample
	LSV_medians = perlsv_coverage_df.apply(median_of_non_negative, axis=1)
	nf_small_df = perlsv_coverage_df.rdiv( LSV_medians, axis=0 )
	nf_df = nf_small_df.loc[ LSVs ].reset_index().drop("LSV", axis=1)

	nf_df[ nf_df < 0 ] = 1 # Ensure missing values remain negative after multiplication by norm factors
	
	return (nf_df.values, coverage_df.multiply( nf_df ).values)


def median_of_non_negative(values_arr):
	# Ignore missing values, which by design are negative
	filtered = values_arr[values_arr >= 0]
	if filtered.empty:
		return 1
	return max(1, np.median(filtered))


def mostly_log_transform(x):

	if x > 2:
		result = math.log(x)
	elif x < 0:
		result = x # missing value
	else:
		result = COEFF_B * (x**2) + COEFF_C * x

	return result


def mostly_log_transform_inverse(x):

	if x > NATURAL_LOG_2:
		result = math.exp(x)
	elif x < 0:
		result = x # missing value
	else:
		# quadratic formula
		result = (1 / (2*COEFF_B)) * (-COEFF_C + math.sqrt( (COEFF_C ** 2) + 4 * COEFF_B * x))

	return result



def LSV_sum_reexpand(df, LSVs):
	"""Return a numpy array

	Assumes the input dataframe has an "LSV" column.
	Groupby this column and sum to aggregate, then re-expand to the original
	dimensions so that each LSV record gets its sum. Return as a numpy array.

	Utility function used to 1) compute total reads over junctions for each LSV, 
	and 2) compute the sum of adjusted PSI in order to renormalize PSI for each LSV
	"""

	df["LSV"] = LSVs
	
	perlsv_df = df.groupby("LSV", sort=False).sum()

	reexp_arr = perlsv_df.loc[ LSVs ].reset_index().drop("LSV", axis=1).values

	return reexp_arr

def LSV_sum_reexpand_api(df, groupings):
    """
    Implementation of the LSV_sum_reexpand, but using generalized groupings instead of LSV definitions
    :param df:
    :param groupings:
    :return:
    """

    # this assembles "LSV" like LSV_sum_reexpand expect's, except they are Intervals instead of lsv-ids
    # the provided functionality is the same, though
    groupings = np.append(groupings, conf.num_juncs)
    breaks = pd.IntervalIndex.from_breaks(groupings, closed='right')
    expanded_ranges = pd.cut(np.arange(1, conf.num_juncs+1), breaks)


    df['LSV'] = expanded_ranges

    perlsv_df = df.groupby("LSV", sort=False).sum()

    reexp_arr = perlsv_df.loc[expanded_ranges].reset_index().drop("LSV", axis=1).values

    return reexp_arr


def print_write(logfile, d, s):
	"""Return None

	Print a string and write it to the logfile.
	"""

	s = str(s)

	if not conf.quiet_mode:
		print(s)

	logfile.write( str(datetime.now()) + "\t" + s + "\n" )

	return None


def debug(s):
	
	# comment out to silence debugging prints
	print(s)

	return None