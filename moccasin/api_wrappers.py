from moccasin.prep_input import get_model_matrix
from moccasin import conf
from moccasin.fit_adjust_api import fitmodel_adjustcounts_api_outer
from multiprocessing import shared_memory, Process, Lock
from multiprocessing import cpu_count, current_process
import numpy as np

class MoccasinModel():

    def __init__(self, model_defs, **kwargs):
        """
        Instantiate a new instance for running a model

        Args:
             model_defs (dict): dict of {sample_name: {modelmat_col: modelmat_col_value}}
        """
        modelmat_df, n_samples, build_model_samples = get_model_matrix(model_dict=model_defs)
        self.modelmat_df = modelmat_df
        self.build_model_samples = build_model_samples
        conf.api_mode = True

        for kwarg in kwargs:
            if hasattr(conf, kwarg):
                setattr(conf, kwarg, kwargs[kwarg])
            else:
                raise Exception(f"Unknown argument key: {kwarg}")



        # no_ruv = conf.RUV_max_num_factors <= 0 and not conf.explore_residual_factors
        # if not no_ruv:
        #     modelmat_df = augment_modelmat_with_unknown_cf_covariates(modelmat_df,
        #                                                               n_samples, build_model_samples)

    def model(self, grouping_data, coverage_data):
        """
        Model one chunk of data and return corrected data

        Args:
            grouping_data (np.array): junction grouping start idxs
            coverage_data (dict): dict of {sample_name:
                np.array of shape (njuncs,),}

        Returns:
            dict of {sample_name: adjusted coverage np.array of shape (njuncs, nbootstraps) }
        """

        output = self.model_with_bootstraps(grouping_data,
                                           {k: v.reshape((v.shape[0], 1,)) for k, v in coverage_data.items()})
        return {k: v[:, 0] for k, v in output.items()}


    def model_with_bootstraps(self, grouping_data, coverage_data):
        """
        Model one chunk of data and return corrected data

        coverage data is two dimensional: (junctions, bootstraps)

        Args:
            grouping_data (np.array): junction grouping start idxs
            coverage_data (dict): dict of {sample_name:
                np.array of shape (njuncs, nbootstraps, )}

        Returns:
            dict of {sample_name: adjusted coverage np.array of shape (njuncs, nbootstraps) }
        """

        # paste all experiment arrays into one 3d block
        full_coverage_array = None
        exp_name_order = []
        for i, exp in enumerate(coverage_data):
            exp_name_order.append(exp)
            if full_coverage_array is None:
                full_coverage_array = np.array([coverage_data[exp]])
            else:
                full_coverage_array = np.vstack([full_coverage_array, [coverage_data[exp]]])


        # copy data into shared block
        cov_shm = shared_memory.SharedMemory(create=True, size=full_coverage_array.nbytes)

        cov_array = np.ndarray(full_coverage_array.shape, dtype=np.float32, buffer=cov_shm.buf)
        cov_array[:] = full_coverage_array[:]  # Copy the original data into shared memory

        conf.cov_shm_name = cov_shm.name
        conf.num_exps = full_coverage_array.shape[0]
        conf.num_juncs = full_coverage_array.shape[1]
        conf.num_bss = full_coverage_array.shape[2]

        grp_shm = shared_memory.SharedMemory(create=True, size=grouping_data.nbytes)

        np_array = np.ndarray(grouping_data.shape, dtype=np.int64, buffer=grp_shm.buf)
        np_array[:] = grouping_data[:]  # Copy the original data into shared memory

        conf.grp_shm_name = grp_shm.name
        conf.num_grps = grouping_data.shape[0]


        try:
            fitmodel_adjustcounts_api_outer(self.modelmat_df, self.build_model_samples, exp_name_order)

            # copy the now adjusted data back from the shared memory to local arrays for each experiment

            output_samples = {}
            for i, exp in enumerate(exp_name_order):

                # need to explicitly copy back from shared memory
                output_chunk = np.ndarray((conf.num_juncs, conf.num_bss), dtype=np.float32)
                output_chunk[:] = cov_array[i, :, :]
                output_samples[exp] = output_chunk



        except:
            raise
        finally:

            cov_shm.close()
            grp_shm.close()

            cov_shm.unlink()
            grp_shm.unlink()

        return output_samples
