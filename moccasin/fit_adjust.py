import pandas as pd
from os.path import join
from moccasin import conf
from moccasin.scf_util import *

mostly_log = np.frompyfunc(mostly_log_transform, 1, 1)
mostly_log_inverse = np.frompyfunc(mostly_log_transform_inverse, 1, 1)

def fitmodel_adjustcounts(chunks_d, modelmat_df, mproc_pool,
	n_bootstraps, build_model_samples):
	"""Return None

	Wrapper for the main model-fitting and adjustment function.
	This function calls fitmodel_adjustcounts_cij, which splits the work by chunk (c),
	bootstrap index (i), and worker (spawned process) (j).
	"""

	print("Computing and applying size factors, modeling, removing confounding varition, and re-applying size factors.")

	cf_str = ", ".join( list( conf.confounding_factors.keys() ) )
	ncf_str = ", ".join( [c for c in modelmat_df.columns if c not in conf.confounding_factors.keys() and c!="filename"] )

	print("Confounding factors: "+cf_str )
	print("Non-confounders: "+ncf_str )

	(linreg_modelmat_df, adjustment_modelmat_df) = get_linreg_modelmat_df(modelmat_df)

	filenames = list(modelmat_df["filename"])

	chunk_idxs_ordered = sorted( chunks_d.keys() )

	n_chunks = len(chunk_idxs_ordered)


	for c in chunk_idxs_ordered:

		worker_LSVs = chunks_d[ c ][ "worker_LSVs" ]


		if conf.adjust_coverage:

			print("Computing LSV coverage adjustments")

			print("Chunk %i of %i "%(c+1, n_chunks))

			all_args = [ (worker_LSVs[j], c, j, filenames, linreg_modelmat_df, adjustment_modelmat_df,
				build_model_samples) for j in range(conf.max_num_processes) ]

			if mproc_pool is not None:
				mproc_pool.map(fitmodel_adjustnreadsnpos_cj, all_args)
			else:
				fitmodel_adjustnreadsnpos_cj( all_args[0] )

		for i in list(range(n_bootstraps)):

			print("Adjusting LSV junction ratios")
			
			print("Chunk %i of %i | Bootstrap index %i of %i"%(c+1, n_chunks, i+1, n_bootstraps))

			all_args = [(worker_LSVs[j], c, i, j, filenames, linreg_modelmat_df, adjustment_modelmat_df,
						 build_model_samples) for j in range(conf.max_num_processes)]

			if mproc_pool is not None:
				mproc_pool.map(fitmodel_adjustcounts_cij, all_args)
			else:
				fitmodel_adjustcounts_cij( all_args[0] )

	return None


def fitmodel_adjustnreadsnpos_cj( ins ):
	"""Return None

	This is the main function for modeling and adjusting num_reads and num_pos
	from the junc_info tables of .majiq files. 
	This is executed only with the -C option.
	For bootstrap experiment i and worker j, the num_reads and num_pos are read in,
	modeled, adjusted, and written out.

	Indexing convention:
	c: the c'th chunk (subset of coverage table rows)
	j: the j'th worker, i.e. the j'th split within chunk c of the rows
	"""

	(LSVs, c, j, filenames, linreg_modelmat_df, adjustment_modelmat_df,
		build_model_samples) = ins

	(nreads_arr, npos_arr) = get_cj_nreadsnpos_arrs(filenames, c, j, len(filenames))

	# model and adjust
	(adjusted_nreads_arr, did_adjust_nreads) = fitmodel_adjust_counts_log_linear(nreads_arr,
		linreg_modelmat_df, adjustment_modelmat_df, build_model_samples)

	adjusted_nreads_arr = adjusted_nreads_arr.astype(np.float64)

	(adjusted_npos_arr, did_adjust_npos) = fitmodel_adjust_counts_log_linear(npos_arr,
		linreg_modelmat_df, adjustment_modelmat_df, build_model_samples)

	adjusted_npos_arr = adjusted_npos_arr.astype(np.float64)

	# clip negative
	adjusted_nreads_arr = np.clip( adjusted_nreads_arr , a_min=0, a_max=None)
	adjusted_npos_arr = np.clip( adjusted_npos_arr , a_min=0, a_max=None)

	### round to ints
	adjusted_nreads_arr = adjusted_nreads_arr.round(0).astype(int)
	adjusted_npos_arr = adjusted_npos_arr.round(0).astype(int)

	# save, for each c j k (chunk, proc, file)
	for k in range(adjusted_nreads_arr.shape[1]):

		adju_cj_pth = join( conf.tmp_dir, "nreads_adju_c_%i_j_%i_k_%i"%(c,j,k) )
		np.save( adju_cj_pth, adjusted_nreads_arr[:,k] )

		adju_cj_pth = join( conf.tmp_dir, "npos_adju_c_%i_j_%i_k_%i"%(c,j,k) )
		np.save( adju_cj_pth, adjusted_npos_arr[:,k] )

		## Don't additionally save did_adjust_nreads or did_adjust_npos since the missing-value data here duplicates coverage

	## save combined nreads table
	# adju_cj_pth = join( conf.tmp_dir, "nreads_adju_c_%i_j_%i_all"%(c,j) )
	# np.save( adju_cj_pth, adjusted_nreads_arr )

	# Check that we actually need all of the above saves

	return None


def fitmodel_adjustcounts_cij( ins ):
	"""Return None

	This is the main function for modeling and adjusting coverage values.
	For bootstrap experiment i and worker j, the coverage counts are read in,
	normalized, modeled, adjusted, reverse-normalized, and written out.

	Indexing convention:
	c: the c'th chunk (subset of coverage table rows)
	i: the i'th bootstrap index out of n_bootstraps
	j: the j'th worker, i.e. the j'th split within chunk c of the rows
	"""

	(LSVs, c, i, j, filenames, linreg_modelmat_df, adjustment_modelmat_df,
		build_model_samples) = ins

	coverage_df = get_cij_coverage_df(filenames, c, i, j, len(filenames))

	##### FUNCTION

	##### Get target coverage for each LSV+sample+bootstrap
	if conf.adjust_coverage:
		# nreads_arr = np.load( join( conf.tmp_dir, "nreads_adju_c_%i_j_%i_all.npy"%(c,j) ) )
		# nreads_df = pd.DataFrame( nreads_arr, columns = coverage_df.columns )

		(adjusted_nreads_arr, did_adjust_coverage) = fitmodel_adjust_counts_log_linear(coverage_df.values,
			linreg_modelmat_df, adjustment_modelmat_df, build_model_samples)

		adjusted_nreads_arr = np.clip(adjusted_nreads_arr, a_min=0, a_max=None)

		nreads_df = pd.DataFrame( adjusted_nreads_arr, columns = coverage_df.columns )
		del adjusted_nreads_arr

		did_adjust_coverage_cij_pth = join( conf.tmp_dir, "did_adjust_coverage_c_%i_i_%i_j_%i"%(c,i,j) )
		np.save( did_adjust_coverage_cij_pth, did_adjust_coverage )

	else:
		nreads_df = coverage_df.copy()

	target_cov_arr = LSV_sum_reexpand(nreads_df, LSVs)

	del nreads_df

	if conf.model_type == 'stzl':
		(nf_arr, coverage_arr) = get_apply_nf(coverage_df, LSVs)
		del nf_arr

		(adjusted_counts_arr, did_adjust_counts) = fitmodel_adjust_counts_log_linear(coverage_arr,
																					 linreg_modelmat_df,
																					 adjustment_modelmat_df,
																					 build_model_samples)

		adjusted_counts_arr = adjusted_counts_arr.astype(np.float64)

	else:
		##### Convert coverage table read rates to psi, for modeling and adjustment input
		norm_arr = LSV_sum_reexpand(coverage_df.copy(), LSVs)
		norm_arr[norm_arr == 0] = 1  # specifically avoid 0/0
		norm_arr[norm_arr < 0] = 1  # ensure psi value is negative in the case of missing values, represented as -1 up to now
		psi_arr = coverage_df.values / norm_arr

		##### Fit models and adjust
		(adjusted_counts_arr, did_adjust_counts) = fitmodel_adjust_counts_log_linear(psi_arr,
																					 linreg_modelmat_df,
																					 adjustment_modelmat_df,
																					 build_model_samples)

		##### clip to 0 and re-normalize so sum_psi=1 per lsv in each sample
		adjusted_counts_arr = np.clip(adjusted_counts_arr, a_min=0, a_max=None)

	# Convert adjusted counts to psi and then get convert psi to the target coverage
	norm_arr = LSV_sum_reexpand(pd.DataFrame( adjusted_counts_arr ), LSVs)
	norm_arr[ norm_arr==0 ] = 1 # specifically avoid 0/0
	adjusted_psi_arr = adjusted_counts_arr / norm_arr

	##### Convert psi to the target coverage
	coverage_arr = adjusted_psi_arr * target_cov_arr

	adju_cij_pth = join( conf.tmp_dir, "bootstrap_adju_c_%i_i_%i_j_%i"%(c,i,j) )
	np.save( adju_cij_pth, coverage_arr )

	did_adjust_counts_cij_pth = join( conf.tmp_dir, "did_adjust_counts_c_%i_i_%i_j_%i"%(c,i,j) )
	np.save( did_adjust_counts_cij_pth, did_adjust_counts )

	return None


def fitmodel_adjust_counts_log_linear(Y, modelmat_df, adjustment_modelmat_df, build_model_samples):

	if conf.common_lsvs_only:
		return fitmodel_adjust_counts_log_linear_common(Y, modelmat_df, adjustment_modelmat_df, build_model_samples)

	return fitmodel_adjust_counts_log_linear_default(Y, modelmat_df, adjustment_modelmat_df, build_model_samples)


def fitmodel_adjust_counts_log_linear_default(Y_input, modelmat_df, adjustment_modelmat_df, build_model_samples ):
	"""Return adjusted counts (numpy array)

	For each junction, fit a linear model on the input (counts or PSI). Compute adjusted counts
	by removing the terms labeled as confounders and re-applying the additive 
	residuals from the full model. Return adjusted counts.

	In the full_correction version: the init_modelmat should include only the 
	confounders. This function adds an intercept to the confounders, and the 
	intercept serves as the only non-confounder. Conceptually this is a more 
	aggressive adjustment since it possibly throws away more variation from the 
	main effects of interest (which are not modeled here, but were possibly used 
	to derive RUV factors).
	"""

	if conf.model_type == 'stzl':
		Y = mostly_log(Y_input.copy())
	else:
		Y = Y_input.copy()

	##### PART 1: BUILD THE MODEL

	M = modelmat_df.values

	# Only build the model with the build_model_samples, which is already a boolean numpy array
	M_build = M[ build_model_samples, : ]
	Y_build = Y[ : , build_model_samples ]

	n_samples_build = Y_build.shape[1]

	controls_designated = n_samples_build < Y.shape[1] # the build_model option was used, i.e. a subset of samples were designated as controls

	#### OLS fit method
	# For each junction, exclude samples with missing values and try to model & adjust

	Mterm = np.linalg.inv( M_build.T @ M_build ) @ M_build.T # default Mterm, to be used for juncs with no missing values

	did_adjust = np.zeros(shape=Y.shape, dtype=np.int8)

	adjusted_arr = Y.copy() # default new values to old values

	n_juncs = Y.shape[0]

	for i in range(n_juncs):

		## Fit the model:

		build_samples_i = [Y_build[i, k] >= 0 for k in range(n_samples_build)] # check for missing values, represented by psi < 0

		control_has_missing_value = ( sum(build_samples_i) < n_samples_build ) and controls_designated
		if control_has_missing_value:
			# Design decision: if the build_model option was used, i.e. control samples were designated, and a control has a missing value, then do not adjust the lsv
			# Log this here: did_adjust is already zeros by default, done and logged
			continue # On to the next junction

		Y_build_i = Y_build[ i , build_samples_i ]

		if sum(build_samples_i) == n_samples_build:
			# No missing values for this junction! We already have this Mterm.
			Mterm_i = Mterm
		else:
			M_build_i = M_build[ build_samples_i, : ]
			try:
				Mterm_i = np.linalg.inv( M_build_i.T @ M_build_i ) @ M_build_i.T
			except np.linalg.LinAlgError as e:
				# Model matrix was no longer full-rank after removing samples with missing values. Cannot adjust this junction.
				# Log this here: did_adjust is already zeros by default, done and logged
				continue # On to the next junction

		params_i = Y_build_i @ Mterm_i.T
		# Done fitting model

		## Adjust values:
		n_samples_adjust = Y.shape[1]
		adjust_samples_i = [Y[i, k] >= 0 for k in range(n_samples_adjust)] # check for missing values, represented by psi < 0

		fit_predictions_i = ( M[ adjust_samples_i, : ] @ params_i.T ).T

		mu_no_confounders_arr_i = ( adjustment_modelmat_df.values[ adjust_samples_i, : ] @ params_i.T ).T

		resid_arr_i = Y[i, adjust_samples_i] - fit_predictions_i

		if conf.model_type == 'stzl':
			adjusted_arr[i, adjust_samples_i] = np.clip(resid_arr_i + mu_no_confounders_arr_i, a_min=0,
														a_max=None)  # update to new values for those samples and juncs which can be adjusted
		else:
			adjusted_arr[i, adjust_samples_i] = resid_arr_i + mu_no_confounders_arr_i  # update to new values for those samples and juncs which can be adjusted

		did_adjust[i, :] += np.asarray([1 if v else 0 for v in adjust_samples_i], dtype=np.int8)

	if conf.model_type == 'stzl':
		adjusted_arr = mostly_log_inverse(adjusted_arr)

	return (adjusted_arr, did_adjust)


def fitmodel_adjust_counts_log_linear_common(Y_input, modelmat_df, adjustment_modelmat_df, build_model_samples ):
	"""Return adjusted counts (numpy array)

	For each junction, fit a linear model on the input (counts or PSI). Compute adjusted counts
	by removing the terms labeled as confounders and re-applying the additive 
	residuals from the full model. Return adjusted counts.

	In the full_correction version: the init_modelmat should include only the 
	confounders. This function adds an intercept to the confounders, and the 
	intercept serves as the only non-confounder. Conceptually this is a more 
	aggressive adjustment since it possibly throws away more variation from the 
	main effects of interest (which are not modeled here, but were possibly used 
	to derive RUV factors).
	"""

	if conf.model_type == 'stzl':
		Y = mostly_log(Y_input.copy())
	else:
		Y = Y_input.copy()

	##### PART 1: BUILD THE MODEL

	M = modelmat_df.values

	did_adjust = np.ones(shape=Y.shape, dtype=np.int8)

	# Only build the model with the build_model_samples, which is already a boolean numpy array
	M_build = M[ build_model_samples, : ]
	Y_build = Y[ : , build_model_samples ]

	#### OLS fit method
	# Mterm = np.matmul( np.linalg.inv( np.matmul(M_build.T, M_build) ), M_build.T )
	Mterm = np.linalg.inv( M_build.T @ M_build ) @ M_build.T
		
	# params = np.apply_along_axis(lambda Yrow: np.matmul(Mterm, Yrow.T), 1, Y_build)
	params = np.apply_along_axis(lambda Yrow: Mterm @ Yrow.T, 1, Y_build)

	del M_build
	del Y_build

	##### PART 2: APPLY THE MODEL

	fit_predictions = ( M @ params.T ).T

	del M

	mu_no_confounders_arr = ( adjustment_modelmat_df.values @ params.T ).T
		
	resid_arr = Y - fit_predictions

	if conf.model_type == 'stzl':
		adjusted_arr = np.clip(resid_arr + mu_no_confounders_arr, a_min=0, a_max=None)
		adjusted_arr = mostly_log_inverse(adjusted_arr)
	else:
		adjusted_arr = resid_arr + mu_no_confounders_arr

	return (adjusted_arr, did_adjust)


def get_linreg_modelmat_df(modelmat_df):
	"""Return a pandas dataframe

	Returns the pandas dataframe to be used as the model for linear regression.
	The included columns depend whether the user selected the full_correction option.
	"""

	linreg_modelmat_df = modelmat_df.copy()

	if not conf.api_mode:
		linreg_modelmat_df = linreg_modelmat_df.drop("filename", axis=1)

	adjustment_modelmat_df = linreg_modelmat_df.copy()

	nrows = adjustment_modelmat_df.shape[0]

	for cf in conf.confounding_factors.keys():

		v = conf.confounding_factors[ cf ]

		adjustment_modelmat_df[ cf ] = [v]*nrows

	if conf.full_correction:

		retain_cols = [c for c in linreg_modelmat_df.columns if c in conf.confounding_factors or c.lower()=="intercept"]

		linreg_modelmat_df = linreg_modelmat_df[retain_cols]

		adjustment_modelmat_df = adjustment_modelmat_df[retain_cols]

	return (linreg_modelmat_df, adjustment_modelmat_df)


def get_cij_coverage_df(filenames, c, i, j, ncols):
	"""Return a pandas dataframe (coverage_df).

	coverage_df: Contains the read rates (i.e. majiq coverage table entries) for 
		bootstrap index i and worker j in chunk c, for all input .majiq files.
	"""

	col_names = []
	coverage_vals = None

	for k in range(len(filenames)):

		f = filenames[k]

		col_names.append( f+"_btsm_%i"%(i) )

		coverage_ckj = np.load( join( conf.tmp_dir, f+"_c_%i_j_%i"%(c,j)+".npy") , mmap_mode="r" )

		if coverage_vals is None:

			nrows = coverage_ckj.shape[0]

			coverage_vals = np.zeros(shape=(nrows, ncols))

		coverage_vals[ : , k ] = coverage_ckj[ : , i ]

	coverage_df = pd.DataFrame( coverage_vals , columns=col_names )

	return coverage_df


def get_cj_nreadsnpos_arrs(filenames, c, j, ncols):

	"""Return a tuple of numpy arrays: (nreads_arr, npos_arr)

	nreads_arr: numreads rows (from junc_info) handled by chunk c, process j.
	npos_arr: numpos rows (from junc_info) handled by chunk c, process j.

	For both output arrays: each row corresponds to a junction, 
	each column corresponds to an input .majiq file.
	"""

	nreads_arr = None
	npos_arr = None

	for k in range(len(filenames)):

		f = filenames[k]

		nreads_ckj = np.load( join( conf.tmp_dir, f+"_nreads_c_%i_j_%i"%(c,j)+".npy") , mmap_mode="r" )

		npos_ckj = np.load( join( conf.tmp_dir, f+"_npos_c_%i_j_%i"%(c,j)+".npy") , mmap_mode="r" )

		if nreads_arr is None:

			nrows = nreads_ckj.shape[0]

			nreads_arr = np.zeros(shape=(nrows, ncols))

			npos_arr = np.zeros(shape=(nrows, ncols))

		nreads_arr[ : , k ] = nreads_ckj

		npos_arr[ : , k ] = npos_ckj

	return (nreads_arr, npos_arr)