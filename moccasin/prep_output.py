from os.path import join

from moccasin.scf_util import *
from moccasin import conf

def get_write_new_majiqs(chunks_d, modelmat_df, transition_d, n_bootstraps):

	if conf.common_lsvs_only:
		return get_write_new_majiqs_common(chunks_d, modelmat_df, transition_d, n_bootstraps)

	return get_write_new_majiqs_default(chunks_d, modelmat_df, transition_d, n_bootstraps)


def get_write_new_majiqs_default(chunks_d, modelmat_df, transition_d, n_bootstraps):
	"""Return None

	I/O function. For each input file, reads in the adjusted counts from each of the workers (j)
	within each of the chunks (c). Assembles those and then outputs an adjusted .majiq file.

	If the -C (adjust coverage) option is selected, that step is completed by this method as well.
	"""

	chunk_idxs_ordered = sorted( chunks_d.keys() )

	print("Replacing adjusted bootstrap sets into file sets and writing new majiqs.")

	for k in modelmat_df.index:

		print("Majiq %i of %i"%(k+1, len(modelmat_df.index)))

		f = modelmat_df.loc[k, "filename"]

		adjusted_k = []
		did_adjust_counts_k = []
		did_adjust_coverage_k = []
		nreads_k = []
		npos_k = []

		for c in chunk_idxs_ordered:

			worker_cov_idxs = chunks_d[ c ][ "worker_coverage_row_idxs" ]

			coverage_k_c_jparts = tuple(
				[ assemble_kcj(c, j, len(worker_cov_idxs[j]), k, n_bootstraps, "bootstrap_adju" )
				for j in range(conf.max_num_processes) ] )

			adjusted_coverage_k_c = np.vstack(coverage_k_c_jparts)

			adjusted_k.append( adjusted_coverage_k_c )

			did_adjust_counts_k_c_jparts = tuple(
				[ assemble_kcj(c, j, len(worker_cov_idxs[j]), k, n_bootstraps, "did_adjust_counts" )
				for j in range(conf.max_num_processes) ] )

			did_adjust_counts_k_c = np.vstack(did_adjust_counts_k_c_jparts)

			did_adjust_counts_k.append( did_adjust_counts_k_c )

			if conf.adjust_coverage:

				chunk_LSVs = [lsv for lsvs_j in chunks_d[ c ][ "worker_LSVs" ] for lsv in lsvs_j]

				nreads_k_c_jparts = tuple( [
				np.load( join( conf.tmp_dir, "nreads_adju_c_%i_j_%i_k_%i.npy"%(c,j,k) ) ) for j in range(conf.max_num_processes)
				] )
				nreads_k_c = np.concatenate(nreads_k_c_jparts)

				nreads_k.append(nreads_k_c)

				npos_k_c_jparts = tuple( [
				np.load( join( conf.tmp_dir, "npos_adju_c_%i_j_%i_k_%i.npy"%(c,j,k) ) ) for j in range(conf.max_num_processes)
				] )
				npos_k_c = np.concatenate(npos_k_c_jparts)
				npos_k.append(npos_k_c)

				did_adjust_coverage_k_c_jparts = tuple(
				[ assemble_kcj(c, j, len(worker_cov_idxs[j]), k, n_bootstraps, "did_adjust_coverage" )
				for j in range(conf.max_num_processes) ] )

				did_adjust_coverage_k_c = np.vstack(did_adjust_coverage_k_c_jparts)

				did_adjust_coverage_k.append( did_adjust_coverage_k_c )

		adjusted_coverage = np.vstack( tuple(adjusted_k) )
		del adjusted_k

		did_adjust_counts = np.vstack( tuple(did_adjust_counts_k) )
		del did_adjust_counts_k

		maj = np.load( join(conf.majiq_build_dir, f+conf.majiq_extension) )

		new_junc_info = maj["junc_info"].copy()
		# Sort all fields of junc_info properly

		if conf.adjust_coverage:

			concat_nreads_k = np.concatenate( tuple(nreads_k) )
			new_junc_info["f3"] = concat_nreads_k[ transition_d[ f ]["mapping"] ]

			concat_npos_k = np.concatenate( tuple(npos_k) )
			new_junc_info["f4"] = concat_npos_k[ transition_d[ f ]["mapping"] ]

		# copy original coverage
		new_coverage = maj["coverage"]

		# where quantifiable was true, overwrite with correctly mapped values
		#new_coverage[transition_d[ f ]["quantifiable"] == 1] = adjusted_coverage[transition_d[ f ]["mapping"][transition_d[ f ]["quantifiable"] == 1], :]
		new_coverage[transition_d[ f ]["quantifiable"]] = adjusted_coverage[ transition_d[ f ]["mapping"], : ]

		out_f = f + conf.majiq_extension
		out_f = out_f.replace(".majiq", ".scf_adjusted.majiq")
		
		outfile = open(join(conf.out_dir, out_f), "wb")
		np.savez_compressed( outfile,
					lsv_types = maj["lsv_types"], # same; order does not matter
					junc_info = new_junc_info,
					coverage = new_coverage, # no further order update, should be right already
					meta = maj["meta"] )
		
		outfile.close()

		outfile_did_adjust = open(join(conf.out_dir, f + ".did_adjust.record"), "wb")

		if conf.adjust_coverage:

			did_adjust_coverage = np.vstack( tuple(did_adjust_coverage_k) )
			del did_adjust_coverage_k

			np.savez_compressed( outfile_did_adjust,
						junc_info = new_junc_info,
						adjusted_psi = np.asarray(did_adjust_counts[ transition_d[ f ]["mapping"], : ], dtype=np.int8),
						adjusted_coverage = np.asarray(did_adjust_coverage[ transition_d[ f ]["mapping"], : ], dtype=np.int8) )

			del did_adjust_coverage

		else:

			np.savez_compressed( outfile_did_adjust,
						junc_info = new_junc_info,
						adjusted_psi = np.asarray(did_adjust_counts[ transition_d[ f ]["mapping"], : ], dtype=np.int8) )

		outfile_did_adjust.close()

		del adjusted_coverage
		del maj

	print("Done writing new majiqs.")

	return None


def get_write_new_majiqs_common(chunks_d, modelmat_df, transition_d, n_bootstraps):
	"""Return None

	I/O function. For each input file, reads in the adjusted counts from each of the workers (j)
	within each of the chunks (c). Assembles those and then outputs an adjusted .majiq file.

	If the -C (adjust coverage) option is selected, that step is completed by this method as well.
	"""

	chunk_idxs_ordered = sorted( chunks_d.keys() )

	print("Replacing adjusted bootstrap sets into file sets and writing new majiqs.")

	for k in modelmat_df.index:

		print("Majiq %i of %i"%(k+1, len(modelmat_df.index)))

		f = modelmat_df.loc[k, "filename"]

		adjusted_k = []
		nreads_k = []
		npos_k = []

		for c in chunk_idxs_ordered:

			worker_cov_idxs = chunks_d[ c ][ "worker_coverage_row_idxs" ]

			coverage_k_c_jparts = tuple(
				[ assemble_kcj(c, j, len(worker_cov_idxs[j]), k, n_bootstraps, "bootstrap_adju" )
				for j in range(conf.max_num_processes) ] )

			adjusted_coverage_k_c = np.vstack(coverage_k_c_jparts)

			if conf.adjust_coverage:

				chunk_LSVs = [lsv for lsvs_j in chunks_d[ c ][ "worker_LSVs" ] for lsv in lsvs_j]

				nreads_k_c_jparts = tuple( [
				np.load( join( conf.tmp_dir, "nreads_adju_c_%i_j_%i_k_%i.npy"%(c,j,k) ) ) for j in range(conf.max_num_processes)
				] )
				nreads_k_c = np.concatenate(nreads_k_c_jparts)

				nreads_k.append(nreads_k_c)

				npos_k_c_jparts = tuple( [
				np.load( join( conf.tmp_dir, "npos_adju_c_%i_j_%i_k_%i.npy"%(c,j,k) ) ) for j in range(conf.max_num_processes)
				] )
				npos_k_c = np.concatenate(npos_k_c_jparts)
				npos_k.append(npos_k_c)

			adjusted_k.append( adjusted_coverage_k_c )

		adjusted_coverage = np.vstack( tuple(adjusted_k) )
		del adjusted_k

		maj = np.load( join(conf.majiq_build_dir, f+conf.majiq_extension) )

		num_kept = adjusted_coverage.shape[0]

		new_junc_info = maj["junc_info"].copy()
		# Sort all fields of junc_info properly
		new_junc_info[ :num_kept ] = new_junc_info[ transition_d[ f ]["keep"] ][ transition_d[ f ]["sort"] ]



		if conf.adjust_coverage:
			new_junc_info[ :num_kept ]["f3"] = np.concatenate( tuple(nreads_k) )
			new_junc_info[ :num_kept ]["f4"] = np.concatenate( tuple(npos_k) ) # Confirm f3 f4
			# new_junc_info[ :num_kept ]["f3"] = np.vstack( tuple(nreads_k) )
			# new_junc_info[ :num_kept ]["f4"] = np.vstack( tuple(npos_k) ) # Confirm f3 f4

		new_junc_info[ num_kept: ] = new_junc_info[ ~transition_d[ f ]["keep"] ]

		new_coverage = maj["coverage"].copy()
		new_coverage[ :num_kept, : ] = adjusted_coverage
		new_coverage[ num_kept:, : ] = maj["coverage"][ ~transition_d[ f ]["keep"], : ]

		# where quantifiable was true, overwrite with correctly mapped values
		new_coverage[transition_d[f]["quantifiable"] == 0] = maj["coverage"][transition_d[f]["sort"][transition_d[f]["quantifiable"] == 0], :]

		out_f = f + conf.majiq_extension
		out_f = out_f.replace(".majiq", ".scf_adjusted.majiq")
		
		outfile = open(join(conf.out_dir, out_f), "wb")
		np.savez_compressed( outfile,
					lsv_types = maj["lsv_types"], # same; order does not matter
					junc_info = new_junc_info,
					coverage = new_coverage, # no further order update, should be right already
					meta = maj["meta"] )
		
		outfile.close()

		del adjusted_coverage
		del maj

	print("Done writing new majiqs.")

	return None


def assemble_kcj(c, j, nrows, k, n_bootstraps, prefix):
	"""Return a numpy array

	adjusted coverage: coverage table with adjusted read rates.

	For input .majiq file k, the adjusted coverage values from chunk c 
	and worker j are assmbled for all bootstrap experiments (i).
	"""

	adjusted_coverage = np.zeros( shape=(nrows, n_bootstraps) )

	for i in range(n_bootstraps):

		npy_cij_pth = join( conf.tmp_dir, prefix + "_c_%i_i_%i_j_%i"%(c,i,j) ) + ".npy"

		npy_cij = np.load( npy_cij_pth, mmap_mode="r" )

		adjusted_coverage[ : , i ] = npy_cij[ : , k ]
	
	del npy_cij

	return adjusted_coverage
