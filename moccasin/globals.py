import os

from moccasin import __version__
from moccasin.scf_args import get_consolidate_arguments_and_vars, get_args

args = get_args(__version__)
d = get_consolidate_arguments_and_vars(args)

for f_dir in [d["out_dir"], d["tmp_dir"]]:
    if not os.path.exists(f_dir):
        os.mkdir(f_dir)

logfile = open(d["logfile"], "w")