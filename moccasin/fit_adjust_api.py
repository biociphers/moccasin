
import pandas as pd
from os.path import join

from moccasin.scf_util import *
from moccasin.fit_adjust import *
from moccasin import conf

from multiprocessing import shared_memory, Process, Lock
from multiprocessing import cpu_count, current_process
import multiprocessing



def fitmodel_adjustcounts_api_outer(modelmat_df, build_model_samples, exp_name_order):


    linreg_modelmat_df, adjustment_modelmat_df = get_linreg_modelmat_df(modelmat_df)


    all_args = [[i, linreg_modelmat_df, adjustment_modelmat_df,
                                       build_model_samples, exp_name_order] for i in range(conf.num_bss)]

    if conf.max_num_processes > 1:
        mproc_pool = multiprocessing.Pool(conf.max_num_processes)

        mproc_pool.map(fitmodel_adjustcounts_api, all_args)

        mproc_pool.close()
    else:
        for args in all_args:
            fitmodel_adjustcounts_api(args)




def get_cij_coverage_df_api(expnames, i):
    """

    read one bootstrap from all experiments, return pandas df
    (shared memory block)

    """

    col_names = []

    existing_shm = shared_memory.SharedMemory(name=conf.cov_shm_name)
    full_shared_arr = np.ndarray((conf.num_exps, conf.num_juncs, conf.num_bss,), dtype=np.float32, buffer=existing_shm.buf)

    for exp_name in expnames:


        col_names.append( exp_name+"_btsm_%i"%(i) )

    coverage_vals = full_shared_arr[:, :, i]


    copied_dset = np.ndarray((conf.num_exps, conf.num_juncs, ), dtype=np.float32)
    copied_dset[:] = coverage_vals[:]


    coverage_df = pd.DataFrame( copied_dset.transpose() , columns=col_names )

    return coverage_df

def save_cij_coverage_df_api(i, coverage_block):
    """
    save one bootstrap from all experiments, write directly to shared memory
    """

    existing_shm = shared_memory.SharedMemory(name=conf.cov_shm_name)
    full_shared_arr = np.ndarray((conf.num_exps, conf.num_juncs, conf.num_bss,), dtype=np.float32, buffer=existing_shm.buf)

    full_shared_arr[:, :, i] = coverage_block



def fitmodel_adjustcounts_api( ins ):
    """Return None

    Similar to fitmodel_adjustcounts_cij, but for API / non tempfile usage

    Indexing convention:
    i: the i'th bootstrap index out of n_bootstraps

    """

    (i, linreg_modelmat_df, adjustment_modelmat_df,
     build_model_samples, exp_name_order) = ins

    # this should load one bootstrap of coverage
    # the shape should be (num_juncs, num_experiments)
    coverage_df = get_cij_coverage_df_api(exp_name_order, i)

    nreads_df = coverage_df.copy()
    existing_shm = shared_memory.SharedMemory(name=conf.grp_shm_name)
    groupings_arr = np.ndarray((conf.num_grps,), dtype=np.int64,
                               buffer=existing_shm.buf)

    target_cov_arr = LSV_sum_reexpand_api(nreads_df, groupings_arr)

    ##### Convert coverage table read rates to psi, for modeling and adjustment input
    norm_arr = LSV_sum_reexpand_api(coverage_df.copy(), groupings_arr)
    norm_arr[norm_arr == 0] = 1  # specifically avoid 0/0
    norm_arr[
        norm_arr < 0] = 1  # ensure psi value is negative in the case of missing values, represented as -1 up to now
    psi_arr = coverage_df.values / norm_arr

    ##### Fit models and adjust
    (adjusted_psi_arr, did_adjust_psi) = fitmodel_adjust_counts_log_linear(psi_arr,
                                                                       linreg_modelmat_df, adjustment_modelmat_df,
                                                                       build_model_samples)

    ##### clip to 0 and re-normalize so sum_psi=1 per lsv in each sample
    adjusted_psi_arr = np.clip(adjusted_psi_arr, a_min=0, a_max=None)

    norm_arr = LSV_sum_reexpand_api(pd.DataFrame(adjusted_psi_arr), groupings_arr)
    norm_arr[norm_arr == 0] = 1  # specifically avoid 0/0
    adjusted_psi_arr = adjusted_psi_arr / norm_arr

    ##### compute adjusted read rates
    coverage_arr = adjusted_psi_arr * target_cov_arr

    save_cij_coverage_df_api(i, coverage_arr.transpose())

    return None

