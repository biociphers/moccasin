import pandas as pd
import sys
from os.path import join
from collections import Counter
from moccasin import conf
from moccasin.scf_util import *




def get_quantifiable_lsvs(
        majiq_np,
        min_reads,
        min_pos):
    """
    Get LSVs that pass quantifiability thresholds from a majiq np object.

    :param majiq_np: npy object, from loading majiq builder file
    :param min_reads: int, minimum # of reads to consider LSV quantifiable
        Set to 0 to turn this filter "off"
    :param min_pos: int, minimum # of read positions to consider LSV quantifiable
        Set to 0 to turn this filter "off"
    :return: quantifiable_lsvs, quantifiable_lsvs_c
        quantifiable_lsvs: unqiue LSV IDs in majiq np order
        quantifiable_lsvs_c LSV ID counter object LSVs preserved ordered too
    """
    df = pd.DataFrame(majiq_np["junc_info"])
    lsv_summary = df.groupby(
        "f0",  # groupby LSV
        sort=False  # preserve lsv order
    ).agg({
        "f3": "sum",  # sum # of reads in all juncs per LSV
        "f4": "sum"  # sum # of positions in all juncs per LSV
    }).reset_index()
    enough_reads = lsv_summary["f3"] >= min_reads
    enough_pos = lsv_summary["f4"] >= min_pos
    quantifiable_bool = enough_reads & enough_pos
    # unique LSVs IDs that are quantifiable
    quantifiable_lsvs = lsv_summary["f0"][quantifiable_bool]
    # Counter obj of quantifiable LSVs, preserving LSV order of df["f0"]
    quantifiable_lsvs_c = Counter(
        list(
            df["f0"][df["f0"].isin(quantifiable_lsvs)]
        )
    )
    return quantifiable_lsvs, quantifiable_lsvs_c


def get_LSVs(modelmat_df):

	if conf.common_lsvs_only:
		return get_LSVs_common(modelmat_df)

	return get_LSVs_default(modelmat_df)


def get_LSVs_default(modelmat_df):
	"""Return a tuple of (LSV_IDs_ordered, junc_row_order_d, n_bootstraps)

	LSV_IDs_ordered: numpy array. Containins LSV IDs (strings)
		The IDs are non-unique, with one occurrence per junction, and all occurences
		of each ID grouped together.

	junc_row_order_d: nested dictionary. LSV_ID : per_lsv_junction_number : row index
		This determines a definitive order for all the LSV junctions across input files.

	n_bootstraps: int. number of majiq builder bootstrap experiments, i.e. number of
		coverage table columns. Assumed to be the same for all input .majiq files.
	"""

	### Get consensus LSVs

	print("Finding LSVs")

	LSVs_d = None
	n_bootstraps = None

	for k in modelmat_df.index:
		
		f = modelmat_df.loc[k, "filename"]

		maj = np.load( join(conf.majiq_build_dir, f+conf.majiq_extension), mmap_mode="r" )

		LSV_IDs_uq, LSV_IDs = get_quantifiable_lsvs(maj, min_reads=conf.minimum_reads, min_pos=conf.minimum_pos)

		if n_bootstraps is None:
			n_bootstraps = maj["coverage"].shape[1] # assumes this is uniform across .majiq files

		if LSVs_d is None:
			LSVs_d = LSV_IDs
		else:
			LSVs_d = {**LSVs_d, **LSV_IDs }

	### Generate a definitive order (index) of the LSVs

	print("Num. LSVs: %i"%( len(LSVs_d.keys()) ) )

	junc_row_order_d = dict()
	LSV_IDs_ordered = []
	i=0
	for lsv in LSVs_d.keys():

		num_juncs = LSVs_d[lsv]

		LSV_IDs_ordered += [lsv]*num_juncs
		
		min_idx = i
		max_idx = i + num_juncs - 1

		junc_row_order_d[ lsv ] = dict()
		junc_row_order_d[ lsv ][ "idxs" ] = np.arange(min_idx, max_idx+1)
		junc_row_order_d[ lsv ][ "n_juncs" ] = max_idx - min_idx + 1

		i += num_juncs


	return (LSV_IDs_ordered, junc_row_order_d, n_bootstraps)


def get_LSVs_common(modelmat_df):
	"""Return a tuple of (common_LSV_IDs_ordered, junc_row_order_d, n_bootstraps)

	common_LSV_IDs_ordered: numpy array. Containins LSV IDs (strings)
		The IDs are non-unique, with one occurrence per junction, and all occurences
		of each ID grouped together.

	junc_row_order_d: nested dictionary. LSV_ID : per_lsv_junction_number : row index
		This determines a definitive order for all the common LSV junctions across input files.

	n_bootstraps: int. number of majiq builder bootstrap experiments, i.e. number of
		coverage table columns. Assumed to be the same for all input .majiq files.
	"""

	### Get consensus LSVs

	print("Finding consensus LSVs")

	common_LSVs_l = None
	n_bootstraps = None

	for k in modelmat_df.index:
		
		f = modelmat_df.loc[k, "filename"]

		maj = np.load( join(conf.majiq_build_dir, f+conf.majiq_extension), mmap_mode="r" )

		if common_LSVs_l is None:
			common_LSVs_l =[ set( maj["lsv_types"]["f0"] ) ]
			n_bootstraps = maj["coverage"].shape[1] # assumes this is uniform across .majiq files
		else:
			common_LSVs_l.append( set( maj["lsv_types"]["f0"] ) )

			# memory optimization, ensure we don't hold too many at once
			if len(common_LSVs_l) > 20:
				common_LSVs_l = [ set.intersection(*common_LSVs_l) ]

	common_LSVs_s = set.intersection( *common_LSVs_l )

	print("Num. consensus LSVs: %i"%( len(common_LSVs_s) ) )
	
	f = modelmat_df.loc[0, "filename"]

	maj = np.load( join(conf.majiq_build_dir, f+conf.majiq_extension), mmap_mode="r" )

	LSV_IDs = maj["junc_info"]["f0"].copy()

	common_juncs = np.asarray( [v in common_LSVs_s for v in LSV_IDs] )

	common_LSV_IDs_ordered = maj["junc_info"]["f0"][ common_juncs ].copy()

	ddd=dict()
	ddd["LSV"] = common_LSV_IDs_ordered
	ddd["idx"] = np.arange(common_LSV_IDs_ordered.shape[0])

	temp_df = pd.DataFrame(ddd, columns=["LSV", "idx"])
	# min_df = temp_df.groupby("LSV").min().sort_values("LSV").reset_index() # sort_values probably redundant, groupby sorts by default
	# max_df =  temp_df.groupby("LSV").max().sort_values("LSV").reset_index() # sort_values probably redundant, groupby sorts by default

	min_df = temp_df.groupby("LSV").min().reset_index() 
	max_df =  temp_df.groupby("LSV").max().reset_index()

	min_arr = min_df[["LSV", "idx"]].values
	max_arr = max_df[["LSV", "idx"]].values

	junc_row_order_d = dict()

	for i in range(min_df.values.shape[0]):
		
		lsv = min_arr[i,0]
		min_idx = min_arr[i,1]
		max_idx = max_arr[i,1]

		junc_row_order_d[ lsv ] = dict()
		junc_row_order_d[ lsv ][ "idxs" ] = np.arange(min_idx, max_idx+1)
		junc_row_order_d[ lsv ][ "n_juncs" ] = max_idx - min_idx + 1


	return (common_LSV_IDs_ordered, junc_row_order_d, n_bootstraps)


def get_transition_d(modelmat_df, order_d, LSVs):

	if conf.common_lsvs_only:
		return get_transition_d_common(modelmat_df, order_d, LSVs)

	return get_transition_d_default(modelmat_df, order_d, LSVs)


def get_transition_d_default(modelmat_df, order_d, LSVs):
	"""Return a dictionary

	The dictionary contains the transition table for each input .majiq file,
	so that modeling takes place with all junction read rates from all files
	in the correct order of junctions.
	"""

	print("Computing reduction and transition tables")

	transition_d = dict()

	L = len(modelmat_df.index)

	for j in range(L):

		filename = modelmat_df.loc[j, "filename"]

		maj = np.load( join(conf.majiq_build_dir, filename + conf.majiq_extension),
			mmap_mode="r" )

		# all LSVs in file
		all_LSV_IDs_uq, all_LSV_IDs = get_quantifiable_lsvs(maj, min_reads=0, min_pos=0)

		# only the quantifiable LSVs in the file
		LSV_IDs_uq, LSV_IDs = get_quantifiable_lsvs(maj, min_reads=conf.minimum_reads, min_pos=conf.minimum_pos)

		# convert series to list
		LSV_IDs_uq = set(LSV_IDs_uq.tolist())

		# Counter of LSV IDs (+1 count for each junction...)
		tot_juncs = sum(LSV_IDs.values())

		trans = np.zeros(tot_juncs, dtype=np.int64)

		quantifiable = np.zeros(sum(all_LSV_IDs.values()), dtype=np.int32)
		i_all = 0
		i=0
		for lsv in LSV_IDs:

			njunc = LSV_IDs[lsv]
			if lsv in LSV_IDs_uq:
				trans[i:(i + njunc)] = order_d[lsv]["idxs"]
				i += njunc
			quantifiable[i_all:(i_all + njunc)] = [1] * njunc
			i_all += njunc

		quantifiable = quantifiable == 1

		transition_d[ filename ] = dict()
		transition_d[ filename ]["mapping"] = trans
		transition_d[filename]["quantifiable"] = quantifiable


	return transition_d


def get_transition_d_common(modelmat_df, order_d, LSVs):
	"""Return a dictionary

	The dictionary contains the transition table for each input .majiq file,
	so that modeling takes place with all junction read rates from all files
	in the correct (common) order of junctions.
	"""

	print("Computing reduction and transition tables")

	transition_d = dict()

	L = len(modelmat_df.index)

	LSVs_s = set( LSVs )

	for j in range(L):
				
		filename = modelmat_df.loc[j, "filename"]
		
		maj = np.load( join(conf.majiq_build_dir, filename + conf.majiq_extension),
			mmap_mode="r" )
		
		LSV_IDs = maj["junc_info"]["f0"].copy()

		common_juncs = np.asarray( [v in LSVs_s for v in LSV_IDs] )

		LSV_IDs = maj["junc_info"]["f0"][ common_juncs ].copy()

		LSV_IDs_uq = np.asarray( pd.Series( LSV_IDs ).unique() )

		# only the quantifiable LSVs in the file
		quant_LSV_IDs_uq, quant_LSV_IDs = get_quantifiable_lsvs(maj, min_reads=10, min_pos=3)
		quant_LSV_IDs_uq = set(quant_LSV_IDs_uq)

		n_LSV = LSV_IDs.shape[0]
		
		trans = np.zeros( n_LSV )
		quantifiable = np.zeros(n_LSV)

		i=0
		for lsv in LSV_IDs_uq:
			n_juncs = order_d[ lsv ]["n_juncs"]
			trans[i:(i+n_juncs)] = order_d[ lsv ]["idxs"]
			quantifiable[i:(i + n_juncs)] = lsv in quant_LSV_IDs_uq
			i+=n_juncs
		
		transition_d[ filename ] = dict()
		transition_d[ filename ]["keep"] = common_juncs
		transition_d[ filename ]["sort"] = np.argsort( trans )
		transition_d[filename]["quantifiable"] = quantifiable
		
		#LSV_IDs = LSV_IDs[ transition_d[ filename ]["sort"] ] # needed?

		
	return transition_d

def get_model_matrix(file_path=None, model_dict=None):
    """Return the model matrix (pandas dataframe)

    Read in the model matrix tsv input by the user.
    Check whether it is full-rank and whether the indicated
    confounding factors exist as column names, exit if not.
    """

    print("Loading model matrix")

    if file_path:
        modelmat_df = pd.read_csv(file_path, sep="\t")
    else:
        modelmat_df = pd.DataFrame.from_dict(model_dict).transpose()

    #modelmat_df.rename(index=0, inplace=True)


    n_samples = modelmat_df.shape[0]

    build_model_samples = np.asarray( [True]*n_samples )

    if "build_model" in modelmat_df.columns:

        modelmat_df["build_model"] = [int(v) for v in modelmat_df["build_model"]]

        build_model_samples = np.asarray( modelmat_df["build_model"] == 1 )

        modelmat_df = modelmat_df.drop("build_model", axis=1)

    if file_path:
        mm_df = modelmat_df.drop("filename", axis=1)
    else:
        mm_df = modelmat_df

    ### CHECK FOR NANS
    nansum = np.sum( np.isnan( mm_df.values.flatten() ) ) # flatten probably redundant

    if nansum > 0:

        print(("ERROR: Found NaN in model matrix (count=%i). Please check "
            "that the model tsv is correct. This error can be caused by "
            "extra tabs at the end of lines inducing empty matrix columns. "
            "Program will exit."%(nansum)))

        sys.exit()

    ### CHECK FULL-RANK
    rk = np.linalg.matrix_rank(mm_df.values[build_model_samples, :])

    if rk < mm_df.shape[1]:

        print("ERROR: The model matrix is not full-rank: %i < %i"%(rk, mm_df.shape[1]) )

        print(("A full-rank matrix is required. Note that this refers "
            "to the sub-matrix consisting of build_model rows if that column has been "
            "included.\nProgram will exit."))

        sys.exit()

    ### CHECK THAT KNOWN CF ARE MATRIX COLUMNS
    colnames_s = set( list(mm_df.columns) ) # list cast may be redundant

    for cf in conf.confounding_factors.keys():

        if not cf in colnames_s:

            print(("ERROR: %s was included as a confounding factor argument, "
                "but it is not one of the model matrix column names. "
                "Please check spelling and upper/lower case correctness. "
                "Program will exit."%(cf)))

            sys.exit()


    return (modelmat_df, n_samples, build_model_samples)