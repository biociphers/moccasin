from numpy.linalg import inv
from scipy.linalg import svd

from moccasin.prep_input import *
from moccasin.divide_work import *
from moccasin import conf

def get_std_df(LSVs, coverage_df, offset):
	"""Return a tuple (pandas dataframe, numpy array)

	Helper for augment_modelmat_with_unknown_cf_covariates.

	Outputs a dataframe with standard deviations of normalized
	read rate for the input junction rows, with only the most-varying
	junction for each LSV retained.

	Additionally outputs the normalized coverage read rates.
	"""

	norm_arr = LSV_sum_reexpand(coverage_df.copy(), LSVs)
	norm_arr[ norm_arr==0 ] = 1 # specifically avoid 0/0
	psi_arr = coverage_df.values / norm_arr

	ddd = dict()
	ddd["LSV"] = LSVs
	ddd["row_idx"] = offset + np.asarray( range(psi_arr.shape[0]) )
	ddd["std"] = np.std( psi_arr, axis=1 )

	std_df = pd.DataFrame(ddd, columns=["LSV", "row_idx", "std"])
	del ddd
	std_df = std_df.sort_values("std", ascending=False)
	std_df = std_df.drop_duplicates(subset="LSV", keep="first")

	return (std_df, psi_arr)


def get_ruv_bootstrap_sample_df(modelmat_df, transition_d, keeprows, n_rows, n_cols):
	"""Returns a pandas dataframe

	Helper for augment_modelmat_with_unknown_cf_covariates.
	Loads the median of specified rows of each input majiq file into one column of a numpy array.
	Converts the resulting array into a dataframe and returns this.
	"""

	ruv_bootstrap_sample_arr_c = np.zeros( shape=(n_rows, n_cols) )

	col_names = []

	for k in modelmat_df.index:

		f = modelmat_df.loc[ k, "filename"]

		maj = np.load( join( conf.majiq_build_dir, f+conf.majiq_extension ), mmap_mode="r" )

		coverage_arr_c = maj["coverage"][ transition_d[ f ]["keep"], : ][ transition_d[ f ]["sort"], : ][ keeprows, : ]

		ruv_bootstrap_sample_arr_c[:, k] = np.median( coverage_arr_c, axis=1 ) # median bootstrap

		col_names.append( f+"_median" )

	# ruv_bootstrap_sample_arr_c += conf.PSEUDO_COUNTS

	coverage_df = pd.DataFrame( ruv_bootstrap_sample_arr_c , columns=col_names )

	return coverage_df


def augment_modelmat_with_unknown_cf_covariates(modelmat_df, n_samples, build_model_samples):
	"""Return possibly-augmented model matrix

	This is the main function for augmenting the user-supplied confounding factors with covariates
	from unknown confounding factors discovered using RUV.

	A "median" sample is taken over all the bootstrap experiments for each input file.
	This representative sample is per-sample-per-lsv normalized (to PSI) and then input to RUV.
	The desired number of confounding factor covariates are discovered and added as additional
	columns to the model matrix.

	The flow of the function is:
	- First, the highest-variance junction from each LSV is selected as a representative
	- Among those, only up to RUV_max_num_junctions are retained (top-variance junctions are retained)
	- The retained junctions from the median samples are input to RUV
	- RUV_max_num_factors are extracted from RUV and added to the model matrix
	"""

	print("Confounding factor covariates: doing preliminaries.")

	### Regardless of the common_lsvs_only setting, unknown confounders computation use only LSVs common to all input samples

	(common_LSVs, junc_row_order_d, n_bootstraps) = get_LSVs_common(modelmat_df)

	transition_d = get_transition_d_common(modelmat_df, junc_row_order_d, common_LSVs)

	num_common_juncs = len(common_LSVs)

	chunks_d = get_worker_coverage_row_idxs(common_LSVs,
		num_common_juncs, n_samples)

	print("Augmenting model matrix with unknown confounding factor covariates.")

	### From the median samples, choose junctions with high variance on the (normalized) read rates.

	keep = []

	chunk_idxs_ordered = sorted( chunks_d.keys() )
	n_chunks = len(chunk_idxs_ordered)

	for c in chunk_idxs_ordered:

		print("Computing variances for chunk %i of %i."%(c+1, n_chunks))

		s = chunks_d[ c ][ "start_idx" ]
		e = chunks_d[ c ][ "end_idx" ]

		chunk_njuncs = e-s+1

		coverage_df = get_ruv_bootstrap_sample_df(modelmat_df,
			transition_d, np.arange(s, e+1), chunk_njuncs, modelmat_df.shape[0] )

		(std_df, psi_arr) = get_std_df(common_LSVs[ s:(e+1) ], coverage_df, s)
		del coverage_df
		del psi_arr

		keep.append( std_df )

	keep_df = pd.concat( keep )

	if keep_df.shape[0] <= conf.RUV_max_num_junctions:
		keep_LSVs = keep_df["LSV"]
	else:
		keep_df = keep_df.sort_values("std", ascending=False)
		keep_LSVs = keep_df["LSV"][ :conf.RUV_max_num_junctions ]

	keep_LSVs_s = set( keep_LSVs )

	keep_LSVs_tf = np.asarray( [v in keep_LSVs_s for v in common_LSVs] )
	del keep_LSVs_s

	### Having selected the junction indices, now assemble the junctions into a table for RUV

	print("Assembling final table for RUV input.")

	coverage_df = get_ruv_bootstrap_sample_df(modelmat_df,
			transition_d, keep_LSVs_tf, np.sum(keep_LSVs_tf), modelmat_df.shape[0] )

	(std_df, psi_arr) = get_std_df(common_LSVs[ keep_LSVs_tf ], coverage_df, 0)
	del coverage_df

	psi_arr_reduced = psi_arr[ std_df["row_idx"], : ]

	filenames = modelmat_df["filename"].copy()
	modelmat_df = modelmat_df.drop("filename", axis=1)

	modelmat_df = RUV_imitator(psi_arr_reduced, modelmat_df, build_model_samples)
	del psi_arr_reduced

	cols = modelmat_df.columns
	modelmat_df["filename"] = filenames
	modelmat_df = modelmat_df[["filename"]+list(cols)]


	return modelmat_df


def RUV_imitator(Y_RUV, init_modelmat_df, build_model_samples):
	"""Output the model matrix

	This function executes one of two RUV-inspired procedures to
	discover factors of variation in the data which are not explained 
	by the factors in the model matrix input by the user. It then appends
	the new factors to the user's model matrix and keeps the result if the
	matrix remains full-rank.

	If the user has included a build_model column, indicating a wish to model
	variation using only control samples, then a procedure similar to RUVs is
	utilized: regression is performed on the control sample counts to remove effects 
	from the known confounding factors, and then svd is performed to discover 
	additional factors of unwanted variation.

	Otherwise, a procedure similar to RUVr is utilized: regression is performed on
	all the samples to remove effects from the known confounding factors and main
	variables of interest, and then svd is performed to discover additional factors.

	Please see the paper methods for additional details.
	"""

	# Note: Y_RUV is njuncs x nsamples
	# It is not logged here and may be e.g. PSI values in [0,1]

	M = init_modelmat_df.values

	build_model_with_controls = sum(build_model_samples) < len(build_model_samples)

	if build_model_with_controls:

		# RUVs-like procedure plus initial residualizing step
		M_build = M[ build_model_samples, : ]
		Y_build = Y_RUV[ : , build_model_samples ]

		Mterm = inv( M_build.T @ M_build ) @ M_build.T
				
		params = np.apply_along_axis(lambda Yrow: Mterm @ Yrow.T, 1, Y_build)

		Y_pred = ( M_build @ params.T ).T

		Y_resid = ( Y_build - Y_pred ).T

		Y_resid_centered = Y_resid - np.mean(Y_resid, axis=0, keepdims=True)

		(U, S, Vh) = svd(Y_resid_centered, full_matrices=False) # from scipy.linalg import svd

		A = np.diag(S) @ Vh

		W = Y_RUV.T @ A.T @ inv(A @ A.T) # from numpy.linalg import inv

	else:

		# RUVr-like procedure with all samples
		Mterm = inv( M.T @ M ) @ M.T

		params = np.apply_along_axis(lambda Yrow: Mterm @ Yrow.T, 1, Y_RUV)

		Y_pred = ( M @ params.T ).T

		Y_resid = ( Y_RUV - Y_pred ).T

		# (U, S, Vh) = svd(Y_resid, full_matrices=False) # from scipy.linalg import svd
		# W = U.copy()
		(W, S, Vh) = svd(Y_resid, full_matrices=False) # from scipy.linalg import svd

	# Select up to conf.RUV_max_num_factors
	if W.shape[1] > conf.RUV_max_num_factors:

		W = W[:, :conf.RUV_max_num_factors ]

	# Explore
	if conf.explore_residual_factors:
		
		SS = np.power(S, 2)

		dd = dict()
		dd["prop_var"] = SS / np.sum(SS)
		dd["cum_var"] = np.cumsum(dd["prop_var"])

		prev_df = pd.DataFrame(dd, columns=["prop_var", "cum_var"])

		prev_df["prop_var"] = ["%.3f"%v for v in prev_df["prop_var"]]
		prev_df["cum_var"] = ["%.3f"%v for v in prev_df["cum_var"]]

		print(prev_df)
		
		sys.exit()


	W_df = pd.DataFrame( W, columns=["W_"+str(i+1) for i in range(W.shape[1])] )

	modelmat_df = pd.concat( [init_modelmat_df, W_df], axis=1 )

	# Check rank of augmented model matrix 
	rk = np.linalg.matrix_rank(modelmat_df.values[build_model_samples, :])

	if rk < modelmat_df.shape[1]:

		print("The augmented model matrix is not full-rank: %i < %i"%(rk, modelmat_df.shape[1]) )

		print("A full-rank matrix is required. Note that this refers \
			to the sub-matrix consisting of build_model rows if that column has been \
			included.\nProgram will proceed with the original model matrix.")

		return init_modelmat_df

	print("Added scores from %i RUV factors to model matrix"%(len(W_df.columns)) )

	for c in W_df.columns:

		conf.confounding_factors[c] = 0 # better choice?

	return modelmat_df