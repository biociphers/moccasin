from os import cpu_count
from os.path import join
from math import ceil

from moccasin.scf_util import *
from moccasin import conf

def split_LSV_juncs_into_parts(LSVs, n_parts, offset):
	"""Return a tuple (part_coverage_row_idxs, part_LSVs)

	Helper function for splitting up the work handled in different chunks
	or processes. All junctions of any given LSV are handled in the
	same group.

	part_coverage_row_idxs: list of lists containing the
	(post-transition-table-sorted) coverage table row indices handled
	by each chunk or process.

	part_LSVs: list of lists containing the LSV IDs correspnding
	to the coverage table row indices in part_coverage_row_idxs.
	(This will contain duplicates since each LSV contains 2+ juncs.)
	"""

	n_juncs = len(LSVs)

	if n_parts == 1:

		part_coverage_row_idxs = [ list( range(offset, offset+n_juncs) ) ]

		return (part_coverage_row_idxs, [LSVs])

	cut_idxs = [ int( (i+1) * n_juncs / n_parts ) for i in range(n_parts) ]

	for i in range(len(cut_idxs)):

		j = cut_idxs[i]

		# Never correct the final index
		if j != len(LSVs):

			while LSVs[j-1] == LSVs[j]:
				j -= 1

			cut_idxs[i] = j

	cut_idxs = [0] + cut_idxs

	R = range( len(cut_idxs) - 1 )

	part_coverage_row_idxs = [ list(range( offset+cut_idxs[i], offset+cut_idxs[i+1] )) for i in R ]

	part_LSVs = [ LSVs[ cut_idxs[i] : cut_idxs[i+1] ] for i in R ]

	return (part_coverage_row_idxs, part_LSVs)



def get_worker_coverage_row_idxs(LSVs, n_juncs, n_samples):
	"""Return a nested dictionary

	Each key is a chunk index. The dictionary for each chunk index contains two lists:

	worker_coverage_row_idxs: python list. contains coverage table row indexes for each worker
		(worker in the sense of spawned processes for multiprocessing mode.)

	worker_LSVs: python list. LSV IDs corresponding to each coverage table row for each worker.

	So, the junction rows are partitioned by chunk index; within each chunk the rows are
	possibly partitioned among spawned processes.
	"""

	print("Getting worker coverage row indices")

	# First break into chunks as needed to limit memory overhead
	# Workers (spawned processes) will work on one chunk at a time

	tot_n_cells = int( n_juncs * n_samples )

	n_chunks = int( ceil( tot_n_cells / conf.MAX_N_CELLS_PER_CHUNK ) )

	print("Number of chunks: %i"%(n_chunks))

	(chunk_coverage_row_idxs, chunk_LSVs) = split_LSV_juncs_into_parts(LSVs, n_chunks, 0)

	## Choose a sensible number of processes based on number of LSVs in each chunk

	chunks_d = dict()

	for c in range(n_chunks):

		chunks_d[c] = dict()

		s = chunk_coverage_row_idxs[c][0]
		e = chunk_coverage_row_idxs[c][-1]

		chunks_d[ c ][ "start_idx" ] = s
		chunks_d[ c ][ "end_idx" ] = e

		LSVs_c = LSVs[ s:(e+1) ]

		chunks_d[ c ][ "LSVs_c" ] = LSVs_c

		chunks_d[ c ][ "n_distinct_LSVs_c" ] = np.unique( LSVs_c ).shape[0]

	min_n_distinct_LSVs = min( [ chunks_d[ c ][ "n_distinct_LSVs_c" ] for c in range(n_chunks) ] )

	for c in range(n_chunks):

		(worker_coverage_row_idxs, worker_LSVs) = split_LSV_juncs_into_parts(
			chunks_d[ c ][ "LSVs_c" ], conf.max_num_processes, chunks_d[ c ][ "start_idx" ])

		chunks_d[ c ][ "worker_coverage_row_idxs" ] = worker_coverage_row_idxs

		chunks_d[ c ][ "worker_LSVs" ] = worker_LSVs

	return chunks_d


def split_coverage_tables(modelmat_df, chunks_d, tot_nrows, transition_d):

	if conf.common_lsvs_only:
		return split_coverage_tables_common(modelmat_df, chunks_d, tot_nrows, transition_d)

	return split_coverage_tables_default(modelmat_df, chunks_d, tot_nrows, transition_d)


def split_coverage_tables_default(modelmat_df, chunks_d, tot_nrows, transition_d):
	"""Return None

	For each input .majiq file, write a .npy file into the tmp directory containing the rows
	for each worker (spawned process).

	Three kinds of npy files are written for each .majiq file: one for the coverage table
	rows (read rates), one for nreads from the junc_info table, and one for npos from
	the junc_info table.
	"""

	print("Splitting coverage tables")

	n_bootstraps = None

	for k in modelmat_df.index:

		print("Majiq %i of %i"%(k+1, modelmat_df.shape[0]))

		f = modelmat_df.loc[ k, "filename" ]

		maj = np.load( join( conf.majiq_build_dir, f+conf.majiq_extension ), mmap_mode="r" )

		if n_bootstraps is None:
			n_bootstraps = maj["coverage"].shape[1] # assumes this is uniform across .majiq files

		coverage_arr = -1 * np.ones(shape=(tot_nrows, n_bootstraps))

		coverage_arr[ transition_d[ f ]["mapping"], : ] = maj["coverage"][transition_d[ f ]["quantifiable"]]


		if conf.adjust_coverage:

			nreads_arr = -1 * np.ones(tot_nrows)

			nreads_arr[ transition_d[ f ]["mapping"] ] = maj["junc_info"]["f3"]

			npos_arr = -1 * np.ones(tot_nrows)

			npos_arr[ transition_d[ f ]["mapping"] ] = maj["junc_info"]["f4"]

		for c in chunks_d.keys():

			worker_cov_idxs = chunks_d[ c ][ "worker_coverage_row_idxs" ]

			for j in range(len(worker_cov_idxs)):

				npy_pth = join( conf.tmp_dir, f+"_c_%i_j_%i"%(c,j) )

				np.save( npy_pth, coverage_arr[ worker_cov_idxs[j], : ] )

				if conf.adjust_coverage:

					npy_pth = join( conf.tmp_dir, f+"_nreads_c_%i_j_%i"%(c,j) )

					np.save( npy_pth, nreads_arr[ worker_cov_idxs[j] ] )

					npy_pth = join( conf.tmp_dir, f+"_npos_c_%i_j_%i"%(c,j) )

					np.save( npy_pth, npos_arr[ worker_cov_idxs[j] ] )

	print("Done splitting coverage tables")

	return None


def split_coverage_tables_common(modelmat_df, chunks_d, tot_nrows, transition_d):
	"""Return None

	For each input .majiq file, write a .npy file into the tmp directory containing the rows
	for each worker (spawned process).

	Three kinds of npy files are written for each .majiq file: one for the coverage table
	rows (read rates), one for nreads from the junc_info table, and one for npos from
	the junc_info table.
	"""

	print("Splitting coverage tables")

	for k in modelmat_df.index:

		print("Majiq %i of %i"%(k+1, modelmat_df.shape[0]))

		f = modelmat_df.loc[ k, "filename" ]

		maj = np.load( join( conf.majiq_build_dir, f+conf.majiq_extension ), mmap_mode="r" )

		coverage_arr = maj["coverage"][ transition_d[ f ]["keep"], : ][ transition_d[ f ]["sort"], : ]

		coverage_arr[transition_d[f]["quantifiable"][transition_d[f]["sort"]] == 0, :] = -1

		juncinfo_arr = maj["junc_info"][ transition_d[ f ]["keep"] ][ transition_d[ f ]["sort"] ]

		for c in chunks_d.keys():

			worker_cov_idxs = chunks_d[ c ][ "worker_coverage_row_idxs" ]

			for j in range(len(worker_cov_idxs)):

				npy_pth = join( conf.tmp_dir, f+"_c_%i_j_%i"%(c,j) )

				np.save( npy_pth, coverage_arr[ worker_cov_idxs[j], : ] )

				if conf.adjust_coverage:

					npy_pth = join( conf.tmp_dir, f+"_nreads_c_%i_j_%i"%(c,j) )

					np.save( npy_pth, juncinfo_arr["f3"][ worker_cov_idxs[j] ] )

					npy_pth = join( conf.tmp_dir, f+"_npos_c_%i_j_%i"%(c,j) )

					np.save( npy_pth, juncinfo_arr["f4"][ worker_cov_idxs[j] ] )

	print("Done splitting coverage tables")

	return None